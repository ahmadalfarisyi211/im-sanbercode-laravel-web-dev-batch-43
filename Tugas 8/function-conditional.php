<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Function dan Conditional</h1>
    <?php
    echo "<h3>SOAL No 1 Greetings</h3>";

    function greetings($nama){
        echo "Hallo $nama , Selamat Datang di Sanbercode! <br>";

    }

    greetings("Bagas");
    greetings("Wahyu");
    greetings("Ahmad Alfarisyi");

    echo "<h3>SOAL No 2 Reverse String </h3>";

    function reverse ($kata1){
        $tampung = "";
        $panjangstr = strlen($kata1);
        for ($i = $panjangstr - 1; $i >= 0; $i--){
            $tampung .= $kata1[$i];
        }
        return $tampung;
    }

    function reverseString($kata2){
        $balikKata = reverse($kata2);
        echo $balikKata ."<br>";
    }

    reverseString("Ahmad Alfarisyi");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";

    echo "<h3>SOAL No 3 Palindrome</h3>";

    function palindrome($kata3){
        $balik = reverse($kata3);
        if($kata3 == $balik){
            echo "$kata3 => true <br>";
        }else{
            echo "$kata3 => false <br>";
        }
    }

    palindrome("civic") ; 
    palindrome("nababan") ; 
    palindrome("jambaban"); 
    palindrome("racecar");
    
    echo "<h3>SOAL No 4 </h3>";

    function tentukan_nilai($nilai)
    {
        if($nilai >= 85 && $nilai <=100){
            return "$nilai => Sangat Baik <br>";
        }else if ($nilai >=70 && $nilai < 85){
            return "$nilai => Baik <br>";
        }else if ($nilai >=60 && $nilai < 85){
            return "$nilai => Cukup <br>";
        }else{
            return "$nilai => Kurang <br>";
        }
    }
    echo tentukan_nilai(98); 
    echo tentukan_nilai(76); 
    echo tentukan_nilai(67); 
    echo tentukan_nilai(43);

    ?>
</body>
</html>