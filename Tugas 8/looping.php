<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Looping</h1>
    <?php
    echo "<h3> SOAL No 1 </h3>";

    echo "<h4> Looping Pertama </h4>";
    for($i = 2; $i <= 20; $i += 2){
        echo $i . " - I Love PHP <br>";
    }

    echo "<h4>Looping Kedua</h4>";
    for ($i = 20; $i > 0; $i -= 2){
        echo $i . " - I Love PHP <br>";
    }

    echo "<h3> SOAL No 2 </h3>";

    $numbers = [18, 45, 29, 61, 47, 34];
    echo "array numbers: ";
    print_r($numbers);

    foreach($numbers as $value){
        $Tampungan [] = $value % 5 ;
    }

    echo "Array sisa bagi 5 adalah : ";
    print_r($Tampungan);

    echo "<h3>SOAL No 3 </h3>" ;

    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach ($items as $arrayIndeks){
        $data = [
            "id" => $arrayIndeks[0],
            "name" => $arrayIndeks[1],
            "price" => $arrayIndeks[2],
            "descripton" => $arrayIndeks[3],
            "source" => $arrayIndeks[4]
        ];
        print_r($data);
        echo "<br>";

    }

    echo "<h3> SOAL No 4 </h3>";

    for($a = 1; $a<= 5; $a++){
        for($b=1; $b<=$a; $b++){
            echo " * ";
        }
        echo "<br>";
    }


    ?>
</body>
</html>