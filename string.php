<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih String</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php
    echo "<h3> SOAL No 1 </h3>";

    $first_sentence = "Hello PHP!";
    echo "kalimat pertama : " . $first_sentence . "<br>";
    echo "Panjang String : " . strlen($first_sentence) . "<br>" ;
    echo "Jumlah Kata : " . str_word_count($first_sentence) . "<br><br>";

    $second_sentence = "I'm ready for the challenges";
    echo "kalimat pertama : " . $second_sentence . "<br>";
    echo "Panjang String : " . strlen($second_sentence) . "<br>" ;
    echo "Jumlah Kata : " . str_word_count($second_sentence) . "<br><br>" ;

    echo "<h3> SOAL No 2 </h3>";

    $string2 = "I Love PHP" ;
    echo "kata pertama : " . substr($string2,0,2) . "<br>";
    echo "Kata Kedua : " . substr($string2,2,4) . "<br>";
    echo "Kata Ketiga : " . substr($string2,7,3) . "<br>";
    

    ?>
</body>
</html>