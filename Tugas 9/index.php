<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold_Blooded : " . $sheep->cold_blooded . "<br><br>";

$kodok = new frog("buduk");

echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->name . "<br>";
echo "Cold_Blooded : " . $kodok->name . "<br>";
echo $kodok-> Jump("Hop-Hop"). "<br><br>";

$sungokong = new ape("kera sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->name . "<br>";
echo "Cold_Blooded : " . $sungokong->name . "<br>";
echo $sungokong-> Yell("Auoo");

?>